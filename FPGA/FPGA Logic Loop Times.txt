myRIO:
ModErrorLoopTime [ticks] = 160 (4 µs)
PIDLoopTime [ticks] = 17 (0.425 µs)
FPGAOutputSampleTime [ticks] = 324 (8.1 µs)

NI 9147:
ModErrorLoopTime [ticks] = x (y µs)
PIDLoopTime [ticks] = x (y µs)
FPGAOutputSampleTime [ticks] = x (y µs)